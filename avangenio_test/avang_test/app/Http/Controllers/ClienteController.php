<?php

namespace App\Http\Controllers;

use App\Models\Cliente;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class ClienteController extends Controller
{
   
    public function index()
    {
        //$c= Cliente::all();
        //return view('clientes')->with('cli',$c);
        return view('game.index');
    }

    
    public function store(Request $request)
    {

        //dd($request->all());

        if($request->__type__=='register'){
        $c=new Cliente;
        $c->usuario=$request->usuario;
        $c->edad=$request->edad;
        $c->status='none';
        $c->tiempo=0.0;
        //dd($c);
        $c->save();
        //return $c;
        return view('game.prueba')->with('_c_',$c);
       }
       else if($request->__type__=='ranking'){
        //dd($request->all());
        $c=Cliente::find($request->id);
        $c->status=$request->status;
        $c->tiempo=($request->tiempo);
        $c->update();
        ////////////////////////////////////
        //$ccc= Cliente::all();
        $a=DB::select("SELECT c.* FROM clientes c WHERE c.status='gano' ORDER by c.tiempo ASC");
        $b=DB::select("SELECT c.* FROM clientes c WHERE c.status='perdio' ORDER by c.tiempo ASC");
        $c=DB::select("SELECT c.* FROM clientes c WHERE c.status='none' ORDER by c.tiempo ASC");
        return view('game.ranking',['_a'=>[$a,$b,$c]]);
       }else if($request->__type__=='index'){
        return view('game.index');
       }
    }

    
    public function show(Cliente $cliente)
    {
        return $cliente;
    }

    
    public function update(Request $request, Cliente $cliente)
    {
        //
    }

   
    public function destroy(Cliente $cliente)
    {
        //
    }
}
