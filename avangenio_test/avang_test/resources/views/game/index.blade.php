<!--<link href="bootstrap.min.css" rel="stylesheet"/>-->
<link href="{{base_path().'\\public\\vendor\\css\\bootstrap.min.css'}}" rel="stylesheet"/>

<form action="" method="post">
  {{ csrf_field() }}
<section class="vh-100" style="background-color: black;">
  <div class="container h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-xl-9">

        <h1 class="text-white mb-4">Jugar Toros y Vacas</h1>

        <div class="card" style="border-radius: 15px;background-color: grey;">
          <div class="card-body">

            <div class="row align-items-center pt-4 pb-3">
              <div class="col-md-3 ps-5">

                <h2 class="mb-0">Usuario</h2>

              </div>
              <div class="col-md-9 pe-5">

                <input name="usuario" id="usuario" type="text" class="form-control form-control-lg" />

              </div>
            </div>

            <hr class="mx-n3">

            <div class="row align-items-center py-3">
              <div class="col-md-3 ps-5">

                <h2 class="mb-0">Edad</h2>

              </div>
              <div class="col-md-9 pe-5">

                <input name="edad" id="edad" type="text" class="form-control form-control-lg" />

              </div>
            </div>

            <hr class="mx-n3">

            <div class="px-5 py-4">
              <input type="hidden" name="__type__" value="register">
              <button type="submit" class="btn btn-primary btn-lg" onclick="" >Aceptar</button>
            </div>

          </div>
        </div>

      </div>
    </div>
  </div>
</section>
</form>

<script>
  function aceptar(){
    var a=document.getElementById('usuario').value;
    var b=document.getElementById('edad').value;
    alert(a+','+b);
  }
</script>