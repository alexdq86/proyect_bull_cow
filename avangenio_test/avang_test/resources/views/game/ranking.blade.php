<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Listado de Ranking</title>
</head>

<body>
  <form action="" method="post">
  {{ csrf_field() }}
    <h1>Ranking!!!!!</h1>
     <div class="px-5 py-4">
              <input type="hidden" name="__type__" value="index">
              <button type="submit" class="btn btn-primary btn-lg" onclick="" >Volver</button>
     </div>
    <div class="card-body">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>Pos</th>
                    <th>Id</th>
                    <th>Nombre</th>
                    <th>Edad</th>
                    <th>Status</th>
                    <th>Tiempo</th>
                </tr>
            </thead>
            <tbody>
              @php($count=1)
              @foreach ($_a as $_c )
                @foreach ($_c as $c )
                    <tr>
                        <td>{{$count}} </td>
                         @php($count++)
                        <td>{{$c->id}} </td>
                        <td>{{$c->usuario}} </td>
                        <td>{{$c->edad}} </td>
                        <td>{{$c->status}} </td>
                        <td>{{$c->tiempo}} </td>
                    </tr>
                @endforeach
              @endforeach
            </tbody>
        </table>
        </form>
</body>

</html>
